# FAQ

## No Green Build

In situations where we are waiting for a green build, we have two options,
[bothering people](#notify-others) as necessary and wait, or we can find a
recent green and perform a bit of manual work to get things in sync.  The latter
does not guarantee we'll still end up with a green build in the long run.  The
former may take the longest time to achieve but will result in greater
confidence that we have a good build to pick from.

## Notify Others

Communication is imperitive in this role.  We must be able to overly communicate
and express direct intention as our responsibility to perform a release is our
top priority.  Utilize the following set of guidelines towards discovering where
issues lies and to whom we ask questions.

### General Assistance

When needing help for situations where you need an engineers assistance, such as
merge conflicts, try to identify the people that worked on that commit and ping
them directly in one of the following channels:

* `#development`
* `#backend`
* `#frontend`

### Failed Builds

In the `#development` slack channel we have alerts when builds fail.  This is a
great place to start and starting to ask questions.  It's helpful if you can
find a targeted commit that might have started the red build and ping persons
directly associated with that commit.

# Common Terms

## Feature Freeze Date

This is the deadline for which Feature Development makes it into a Release.  We
utilize the 7th of the month as our target for this date.  Any feature work that
did not make it into master by this date will either be delayed into the next
release, or must [go through our exception request process.](./general/exception-request/process.md)

## Preparation MR

These are utilized to track changes between Release Candidates.  The preparation
branch is first created off the current `X-Y-stable` branch, and all changes
would be plucked into this branch and later merged after a successful build.

Common noted in the format
* `X-Y-stable-prepration-RCZ`
* `X-Y-stable-ee-prepration-RCZ`

Examples:
* `9-0-stable-prepration-RC3`
* `9-0-stable-ee-prepration-RC3`
