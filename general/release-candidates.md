# Creating Release Candidates

Release Candidates (RCs) are pre-release versions of the next major version of
GitLab CE and EE. The first RC, appropriately called RC1, is typically created
as early as possible during the first week of each month. This allows us to
start QA before the feature freeze.

Every release will have several RCs, and there is no limit on the final number.
Usually, at least four RCs are made before the official release. This ensures
new changes are tried in production, at scale, and new bugs can be fixed before
the official release.

There should be no code changes between the final RC and what is released to the
public.

Usually RC1 has the most number of migrations that should be deployed into
production as soon as possible. Release managers should prioritize getting RC1
deployed. If there are showstopping issues with RC1 (e.g. failed migrations,
critical bugs that prevent logins, etc.), a new RC2 should be created
that contains the **minimal set of changes** needed to deploy the
package. Avoid picking more patches that may slow down the release, especially
those that contain new migrations.

## About the "Release Candidate" naming

We call them "Release Candidates" even though the early RCs are closer to Beta
than real RC. This simplifies our releasing/packaging tools and scripts. This
approach is coherent with packages.gitlab.com since our RC packages are
available under [`gitlab/unstable`].

## Guides

- [Creating an RC prior to feature freeze](#prior-to-feature-freeze)
- [Feature freeze RC](#feature-freeze-rc)
- [Creating subsequent RCs](#creating-subsequent-rcs)

### Prior to Feature Freeze

A release issue should already be created.  Follow the monthly guide if it does
not already exist: [Monthly Release](monthly.md)

#### Step 1: Pick a commit on gitlab-ce master branch

(There is a plan to automate this step: https://gitlab.com/gitlab-org/release-tools/issues/245)

Select a commit on [gitlab-ce master
branch](https://gitlab.com/gitlab-org/gitlab-ce/commits/master) which will be
used as the "cut" for this release (everything up to this commit in `master`
branch will be included in the release). If this RC is being done next morning
after the feature freeze you can use just `master` for merging new code into
preparation branches.  It is advised to make the cut from a recent green build
as to avoid problems during pipeline test failures.

#### Step 2: Integrating changes from `master` into `X-Y-stable`

##### RC1

If this is the first RC we need to create our stable branch

```
git checkout <chosen sha of step 1>
git checkout -b X-Y-stable
git push -u origin X-Y-stable
```

Once the `X-Y-stable` branch is created, it is the sole source of future
releases for that version. From the 8th, merge requests will either
be [cherry-picked] into `X-Y-stable` by the release manager, or a second merge
request targeting `X-Y-stable` (instead of `master`) should be opened.

Developers are responsible for notifying the release manager that a merge
request is ready to be moved into `X-Y-stable` by following the ["After the
7th" process].

##### RCX (prior to or on feature freeze date)

If this is not the first RC we simply need to update our stable branch

  **Note:** Ensure the master branch contains green builds. [What to do when
  master does not have a green build](./faq.md#no-green-builds)

```
# checkout to the prepration branch for the current RC in gitlab-ce
git checkout X-Y-stable
git merge master --no-ff
git push

# and for EE branch in gitlab-ee
git checkout X-Y-stable-ee
git merge master --no-ff
git push
```

And also cherry-pick MRs created in step 4 (`.gitignore` and `Dockerfile` changes) and step 4 (licenses).

#### Step 3: Update the `.gitignore`, and `Dockerfile` templates on `gitlab-ce`

Run `bin/rake gitlab:update_templates` and open a merge request to `master`

This will update the [`.gitignore`](https://github.com/github/gitignore),
and [`Dockerfile`](https://gitlab.com/gitlab-org/Dockerfile) templates.

Ensure the MR is merged and marked Pick into X.Y.

#### Step 4: Update the dependencies license list on `gitlab-ce`

Run `bin/bundle exec license_finder report --format=csv --save=vendor/licenses.csv` and
open a merge request to `master`.

Ensure the MR is merged and marked Pick into X.Y.

Cherry-pick both MRs into preparation branches.

#### Step 5: Sync omnibus `X-Y-stable` & `X-Y-stable-ee` with master

```sh
git checkout master
git pull origin
git checkout -b X-Y-stable-prepare
git push -u origin
git checkout master
git checkout -b X-Y-stable-ee-prepare
git push -u origin
```
- Create new merge request for the newly created branches targeting `X-Y-stable` & `X-Y-stable-ee` respectively.
- Merge the MRs

---

### Feature Freeze RC

This is the first RC after the [Feature Freeze](./faq.md#feature-freeze-date).

#### Step 1: Pick a "freeze" commit on gitlab-ce master branch

(There is a plan to automate this step: https://gitlab.com/gitlab-org/release-tools/issues/245)

Select a commit on [gitlab-ce master
branch](https://gitlab.com/gitlab-org/gitlab-ce/commits/master) which will be
used as the "cut" for this release (everything up to this commit in `master`
branch will be included in the release). If this RC is being done next morning
after the feature freeze you can use just `master` for merging new code into
preparation branches.

> **Note:** If this RC is being done with some delay you may want to use
an older commit (closer to the freeze time). Copy the commit's SHA (which you
want to use as the "cut") and pick the freeze commit on EE master branch (it's
possible the master CE was not synced to EE yet, so it might be an older or
EE-only commit SHA, which is not a problem - CE is synced to EE when preparing
RC preparation MRs). Then update the channel message in step 2 bellow - not
everything 'after this point' but everything 'after the freeze commit' will go
into next release. And in step 3, when merging CE and EE into preparation
branches, use these freeze commits instead of `master`.

#### Pick up at step 2 above

Start at the second section of step two where we need to update our stable
branch.

---

### Creating subsequent RCs

Create a release issue by running the following in Slack:

```sh
/chatops run release_issue MAJOR.MINOR.0-rcX
```

Replace `MAJOR.MINOR` with the major and minor version, and `X` with the RC
version. For example:

```sh
/chatops run release_issue 11.8.0-rc2
```

Once created, follow the steps outlined in the release issue.

[`gitlab/unstable`]: https://packages.gitlab.com/gitlab/unstable
[cherry-picked]: pick-changes-into-stable.md

---

[Return to Guides](../README.md#guides)
